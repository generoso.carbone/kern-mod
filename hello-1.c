#include <linux/module.h>
#include <linux/kernel.h>

/**
 * Moduli kernel devono avere almeno due funzioni:
 * init_module() chiamata quando il modulo viene caricato
 * (ismoded) nel kernel
 * cleanup_module() chiamata quando il modulo viene rimosso
 * dal kernel (rmmoded)
 *
 * A partire dal kernel 2.3.13 possiamo dargli qualsiasi nome
 * vogliamo.
 * 
 * Tipicamente init_module
 *  registra un handler per qualcosa nel kernel
 * OPPURE
 *  rimpiazza una delle funzioni kernel con il proprio codice
 * cleanup_module dovrebbe invertire quello fatto da 
 * init_module
 */
int init_module(void)
{
	printk(KERN_INFO "Salut, Mundi 1.\n");
	return 0; //non-zero init_module failed
}

void cleanup_module(void)
{
	//meccanismo di logging del kernel
	//8 livelli di priorità
	//il messaggio verrà appeso in /var/log/kern.log
	printk(KERN_INFO "Goodbye world 1.\n");
}
